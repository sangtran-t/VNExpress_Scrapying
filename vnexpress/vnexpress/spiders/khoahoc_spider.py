# -*- coding: utf-8 -*-
from __future__ import print_function
from scrapy.spiders import CrawlSpider, Rule
from scrapy.linkextractors import LinkExtractor
from ..items import VnexpressNews

import scrapy
import logging


class KhoahocSpider(CrawlSpider):
    name = 'khoahoc_spider'
    allowed_domains = ['vnexpress.net']
    start_urls = ['https://vnexpress.net/khoa-hoc']

    rules = [
        Rule(LinkExtractor(deny='/infographics/'), callback='parse')
    ]
    # Get list link in page for crawl data
    @staticmethod
    def get_link_will_crawl(response):
        return response.xpath("//section[@class='sidebar_1']/article/h4//a[1]/@href").extract()

    # Detail of a article
    @classmethod
    def get_detail_of_article(cls, response):
        article = VnexpressNews()

        article['link'] = response.url
        article['tags'] = response.xpath('//ul[contains(@class, "breadcrumb")]/li/h4/a/text()').extract()
        article['time_created'] = response.xpath('//span[@class="time left"]/text()').extract_first()
        article['title'] = response.xpath(
            '//section[@class="sidebar_1"]/h1[contains(@class, "title_news_detail")]/text()').extract_first()
        article['description'] = response.xpath(
            '//section[@class="sidebar_1"]/p[@class="description"]/text()').extract()
        article['content'] = response.xpath(
            '//section[@class="sidebar_1"]/article[contains(@class, "content_detail fck_detail")]/p/text()').extract()
        return article if len(str(article['content'])) >= 500 else logging.info('Not enough contents')

    # get page to crawl
    @staticmethod
    def get_next_page_will_crawl(response):
        main_link = 'https://vnexpress.net'
        # page_active = response.xpath('//div[@id="pagination"]/a[contains(@class, "active")]/@href').extract_first()
        page_prev = response.xpath('//div[@id="pagination"]/a[contains(@class, "prev")]/@href').extract_first()
        page_next = response.xpath('//div[@id="pagination"]/a[contains(@class, "next")]/@href').extract_first()
        prev_page_will_crawl = (main_link.strip() + str(page_prev).strip()) or ''
        next_page_will_crawl = (main_link.strip() + str(page_next).strip()) or ''
        return [prev_page_will_crawl, next_page_will_crawl]

    # log error
    def errback_httpbin(self, failure):
        self.logger.error(repr(failure))

    def parse(self, response):
        crawled = []
        list_links = self.get_link_will_crawl(response)
        for link in list_links:
            if link not in crawled:
                self.log('Visiting: ' + link)
                yield scrapy.Request(url=link, callback=self.get_detail_of_article, errback=self.errback_httpbin)
                crawled.append(link)
            else:
                logging.info("\""+link+"\""+" ==> WAS CRAWLED!!!")
        # follow pagination
        next_page_will_crawl = self.get_next_page_will_crawl(response)
        if next_page_will_crawl[1]:
            yield scrapy.Request(url=next_page_will_crawl[1], callback=self.parse)

