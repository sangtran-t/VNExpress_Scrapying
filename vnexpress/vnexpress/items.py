# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# https://docs.scrapy.org/en/latest/topics/items.html

import scrapy


class VnexpressNews(scrapy.Item):
    # define the fields for your item here like:
    link = scrapy.Field()
    tags = scrapy.Field()
    time_created = scrapy.Field()
    title = scrapy.Field()
    description = scrapy.Field()
    content = scrapy.Field()
